// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBf5kabC4b6C30UHKKl2fkASHUeCSrWCCk",
  authDomain: "psiaapka.firebaseapp.com",
  databaseURL: "https://psiaapka.firebaseio.com",
  projectId: "psiaapka",
  storageBucket: "psiaapka.appspot.com",
  messagingSenderId: "508224690915",
  appId: "1:508224690915:web:6f69ef18e77edd27ea03d6",
  measurementId: "G-R3GPX6P151"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

export {app, analytics};