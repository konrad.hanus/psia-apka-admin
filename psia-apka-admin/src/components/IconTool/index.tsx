import IconButton from '@mui/material/IconButton';
import { OnClick } from '../Map/types';

interface IconToolProps {
  ariaLabel: string;
  onClick: OnClick;
  image: string;
  currentTool: string;
  name: string;
}


const IconTool = (props: IconToolProps) => {

  const isSelected: object = (props.currentTool === props.name) ? { 
    backgroundColor: 'yellow'
  } : {}

    return (<div
          style={{ 
            ...isSelected, 
            zIndex: 1001,
          }}
          data-qa='icon-tool'
          >
          <IconButton color="primary" aria-label={props.ariaLabel} onClick={props.onClick}>
            <img src={props.image} style={{ width: 50 }} />
          </IconButton>
        </div>);

}

export default IconTool;