import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import * as React from 'react';
import { useMapEvents } from 'react-leaflet';
import { LatLng } from '../Map/types';

export default function CityButtons(props: any) {

    const Otwock: LatLng = {
        lat: 52.09085906604856,
        lng: 21.251091364603443
      }
    
      const Wroclaw: LatLng = {
        lat: 51.086534197579525,
        lng: 17.052893843142982,
      }

      const JeleniaGora: LatLng = {
        lat: 50.92219015522075, 
        lng: 15.757605439528342,
      }

      const Lublin: LatLng = {
        lat: 51.24803689849934, 
        lng: 22.56788653046576,
      }


      const [position, setPosition] = React.useState(null);
      
      const map = useMapEvents({});

      const onClick = (city: LatLng) => {
        props.onClickCoursor();
        props.setLocation(city);
        map.flyTo([city.lat, city.lng], 18)
      }
  


  return (
    <div className={props.className} data-qa="city-buttons">
        <div style={{  
            position: "absolute",
            top: "1rem",
            right: "0",
            zIndex: 2147483647,
            color: "#281414",
            margin: "0",
            padding: "0"
            }}>


        <Box
        sx={{
            display: 'flex',
            border: 0,
            borderColor: 'black',
            flexDirection: 'column',
            alignItems: 'center',
            '& > *': {
            m: 1,
            },
        }}
        >
        <ButtonGroup 
            size="small" 
            aria-label="large button group"
            style={{display: 'block'}}>
            
            <Button key="one" variant="contained" color={props.location.lat === Wroclaw.lat ? "success": "primary"} onClick={()=> onClick(Wroclaw)}>WRO</Button>,
            <Button key="two" variant="contained" color={props.location.lat === Otwock.lat ? "success": "primary"} onClick={()=>onClick(Otwock)}>WAW</Button>,
            <Button key="three" variant="contained" color={props.location.lat === Lublin.lat ? "success": "primary"} onClick={()=>onClick(Lublin)}>LUB</Button>,
            <Button key="four" variant="contained" color={props.location.lat === JeleniaGora.lat ? "success": "primary"} onClick={()=>onClick(JeleniaGora)}>JEL. GÓRA</Button>,
        </ButtonGroup>
        </Box>
        </div>
    </div>
  );
}

