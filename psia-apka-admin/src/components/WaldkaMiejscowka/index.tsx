import {Popup, Marker} from 'react-leaflet';
import Otwock from '../Map/helpers/otwock';

const WaldkaMiejscowka = () => 

<Marker position={[Otwock.lat, Otwock.lng]}>
    <Popup>
    Dokładnie Tutaj Waldek Mieszka
    </Popup>
</Marker>;
  
export default WaldkaMiejscowka;