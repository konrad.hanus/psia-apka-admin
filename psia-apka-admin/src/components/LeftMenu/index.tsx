import styled from 'styled-components';
import LeftMenu from './LeftMenu';

const StyledLeftMenu = styled(LeftMenu)`
    position: absolute;
    z-index: 1000; 
    top: 0;
    left: 0;
    bottom: 0;
    width: 60px;
    padding-top: 80px;
    background-color: #dfdfdf;
    
    @media (max-width: 768px) {
        z-index: 2147483647; 
        top: unset;
        bottom: 0;
        left: 0;
        width: 100%;
        height: 60px;
        padding-top: unset;
        display: flex;
        justify-content: space-around;
        align-items: flex-end;
        align-content: flex-start;
    }
    `;
  
export default StyledLeftMenu;