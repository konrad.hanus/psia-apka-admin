const LeftMenu = (props: any) => 
<div className={props.className} data-qa="menu-boczne">
      {props.children}
</div>;

export default LeftMenu;