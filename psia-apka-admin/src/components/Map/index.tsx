import { useState } from 'react';
import { LatLng, MarkerInterface, MarkerType, OnClick } from './types';
import { useList } from 'react-firebase-hooks/database';
import { ref, set, getDatabase } from 'firebase/database';
import { app as firebaseApp } from '../../firebase';
import CityButtons from '../CityButtons';
import { MapContainer, TileLayer } from 'react-leaflet';
import Wroclaw from './helpers/wroclaw';
import dogGymIcon from './helpers/dogGymIcon';
import dogSpotIcon from './helpers/dogSpotIcon';
import treasureIcon from './helpers/trasureIcon';
import IconTool from '../IconTool';

import DogMarker from '../DogMarker';
import WaldkaMiejscowka from '../WaldkaMiejscowka';
import LeftMenu from '../LeftMenu';
import MapWrapper from '../MapWrapper';
import ClickOnTheMap from '../ClickOnTheMap';
// @ts-ignore
import uuid from 'react-uuid';
import setPublic from './helpers/setPublic';
import toast, { Toaster } from 'react-hot-toast';

const database = getDatabase(firebaseApp);

function Map() {
  
  const [markers, setMarkers] = useState<MarkerInterface[]>([]);
  const [currentTool, setCurrentTool] = useState<MarkerType>(MarkerType.Kursor);
  
  const [dogGymSnapshots, dogGymLoading, dogGymError] = useList(ref(database, '/public/dogGym'));
  
  const [dogSpotSnapshots, dogSpotLoading, dogSpotError] = useList(ref(database, '/public/dogSpot'));
  const [treasureSnapshots, treasureLoading, treasureError] = useList(ref(database, '/public/treasure'));
  const [location, setLocation] = useState<LatLng>(Wroclaw);

  const onClickDogGym: OnClick = ():void => {
  
    setCurrentTool(MarkerType.DogGym);
  };

  const onClickCoursor: OnClick = ():void => {
    setCurrentTool(MarkerType.Kursor);
  };
  const onClickDogSpot: OnClick = ():void => {
    setCurrentTool(MarkerType.DogSpot);
  };
  const onClickTreasure: OnClick = ():void => {
    setCurrentTool(MarkerType.Treasure);
  };

  console.log('dogSnapshot', dogGymSnapshots && dogGymSnapshots.length !== 0 && dogGymSnapshots[0].val())
  toast.success(`Konrad coś dodał`)

 return (<>
 <Toaster />
          <LeftMenu>


          <IconTool 
              onClick={onClickCoursor} 
              ariaLabel={"Dodaj do DogGym"}
              image={"./kursor.png"}
              currentTool={currentTool}
              name="kursor"
                />

            <IconTool 
              onClick={onClickDogGym} 
              ariaLabel={"Dodaj do DogGym"}
              image={"./doggym.png"}
              currentTool={currentTool}
              name="dogGym"
                />
                
            <IconTool 
            onClick={onClickDogSpot} 
            ariaLabel={"Dodaj do DogSpot"}
            image={"./dogspot.png"}
            currentTool={currentTool}
            name="dogSpot"
              />

            <IconTool 
            onClick={onClickTreasure} 
            ariaLabel={"Dodaj do Skarb"}
            image={"./treasure.png"}
            currentTool={currentTool}
            name="treasure"
              />
        </LeftMenu>
     
        <MapWrapper>
          <MapContainer 
            center={[location.lat, location.lng]} 
            zoom={18} 
            scrollWheelZoom={false} 
            style={{ height: window.innerHeight}}
            >
              <ClickOnTheMap setPublic={setPublic} setMarkers={setMarkers} markers={markers} currentTool={currentTool}/>
              <CityButtons 
                setLocation={setLocation} 
                location={location}
                onClickCoursor={onClickCoursor} 
              />
           
            <TileLayer
              attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <WaldkaMiejscowka />

            
            { 
              (
                dogGymSnapshots && 
                dogGymSnapshots.length !== 0 && 
                typeof dogGymSnapshots[0].val() !== 'string'
              ) && 
              <DogMarker loading={dogGymLoading} snapshots={dogGymSnapshots} icon={dogGymIcon} type={MarkerType.DogGym}/>
            }
            <DogMarker loading={dogSpotLoading} snapshots={dogSpotSnapshots} icon={dogSpotIcon} type={MarkerType.DogSpot}/>
            <DogMarker loading={treasureLoading} snapshots={treasureSnapshots} icon={treasureIcon} type={MarkerType.Treasure}/>

           
            
          </MapContainer>
        </MapWrapper>
      </>
  );
}

export default Map;
