export enum Librares
{
  places = 'places',
  drawing = 'drawing',
  geometry = 'geometry',
  localContext = 'localContext'
}

export interface LatLng {
  lat: number;
  lng: number
}

export interface MapStyle {
  width: string;
  height: string
}

export enum MarkerType {
    Kursor = 'kursor', 
    DogSpot = 'dogSpot', 
    DogGym = 'dogGym', 
    Treasure = 'treasure'
}

export interface MarkerInterface {
  lat: number; 
  lng: number;
  time: string;
  type: MarkerType;
}

export type OnClick = () => void;

