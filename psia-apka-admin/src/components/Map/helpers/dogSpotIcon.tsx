import {icon } from 'leaflet'
import displayMarker from './displayMarker';
import { MarkerType } from '../types';

const dogSpotIcon = icon({
    iconUrl: displayMarker(MarkerType.DogSpot),
    iconSize: [60, 60],
    iconAnchor: [30, 30],
    popupAnchor: [-3, -76],
    shadowSize: [68, 95],
    shadowAnchor: [22, 94]
  });
  
export default dogSpotIcon;