import { LatLng } from '../types';

const Wroclaw: LatLng = {
    lat: 51.086534197579525,
    lng: 17.052893843142982,
  }

export default Wroclaw;