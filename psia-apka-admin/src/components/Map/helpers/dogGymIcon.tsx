import {icon } from 'leaflet'
import displayMarker from './displayMarker';
import {  MarkerType } from '../types';

const dogGymIcon = icon({
    iconUrl: displayMarker(MarkerType.DogGym),
    iconSize: [60, 60],
    iconAnchor: [30, 30],
    popupAnchor: [-3, -76],
    shadowSize: [68, 95],
    shadowAnchor: [22, 94]
});

export default dogGymIcon;