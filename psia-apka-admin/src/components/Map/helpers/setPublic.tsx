import { ref, set, getDatabase } from 'firebase/database';
// @ts-ignore
import uuid from 'react-uuid';
import { app as firebaseApp } from '../../../firebase';
// @ts-ignore
import toast from 'react-hot-toast';
import removeItem from '../../DogMarker/helpers/removeItem';

const database = getDatabase(firebaseApp);
function setPublic(
    marker: string,
    who:string, 
    lat: string, 
    lng: string, 
    ) {
      const uuidKey: string = uuid();        

      set(ref(database, 'public/' + marker + '/'+ uuidKey), {
        location: {
        accuracy: 123,
        altitude: 124, 
        altitudeAccuracy: 123,
        heading:  303, 
        latitude: lat,
        longitude: lng,
        speed: 0.57, 
        timestamp: 67
        },
        who, 
        uuid: uuidKey
    }).then(() => {
      toast.success(`Twój ${marker} został zapisany`)
    })
    .catch((error) => {
      toast.error('Nie zapisano')
    });
  }
  export default setPublic;