import {  MarkerType } from '../types';

const displayMarker = (marker: MarkerType) => {
    switch (marker) {
      case MarkerType.DogGym:
        return './doggym.png';
      case MarkerType.DogSpot:
        return './dogspot.png';
      case MarkerType.Treasure:
        return './treasure.png';
      case MarkerType.Kursor:
        return MarkerType.Kursor;
    }
  }

export default displayMarker;