import { LatLng } from '../types';

const Otwock: LatLng = {
    lat: 52.09085906604856,
    lng: 21.251091364603443
  }

export default Otwock;