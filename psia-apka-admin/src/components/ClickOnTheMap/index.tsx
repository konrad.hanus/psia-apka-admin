import { useMapEvents } from 'react-leaflet';
import { MarkerType } from '../Map/types';

const ClickOnTheMap = (props: any) => {
    
    const height:number = 80;
    const width: number = 290;
        useMapEvents({
            click(e) {
                if(props.currentTool !== MarkerType.Kursor)
                {
                    // wycinanie obszaru nieklikalnego menu górnego po prawej stronie
                    if(e.containerPoint.y >= height || window.innerWidth-e.containerPoint.x >= width)
                    {
                        const lat = e.latlng.lat;
                        const lng = e.latlng.lng;

                        props.setPublic(props.currentTool, 'konrad', lat, lng)
                        // alert(`Twoja pozycja to, ${lat}, ${lng}`)
                        props.setMarkers([
                            ...props.markers,
                            { 
                            lat: lat, 
                            lng: lng,
                            time: '1',
                            type: props.currentTool
                        }])
                    }
                }
            },
        })
    
    

    return (<div style={{
        position: 'absolute', 
        top: 0, 
        height: height, 
        width: width,
        right: 0,
        border:1,
        backgroundColor: 'grey', 
        zIndex: 2147483647,
        borderBottomLeftRadius: 20,
        opacity: 0.3}}></div>);
}

export default ClickOnTheMap
