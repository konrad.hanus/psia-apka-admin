import styled from 'styled-components';
import MapWrapper from './MapWrapper';

const StyledMapWrapper = styled(MapWrapper)`
height: ${window.innerHeight}px;`
  
export default StyledMapWrapper;