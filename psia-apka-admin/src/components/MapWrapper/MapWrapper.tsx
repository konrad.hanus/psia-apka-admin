const MapWrapper = (props: any) => 
<div id="map" className={props.className}>
    {props.children}
</div>;
export default MapWrapper
