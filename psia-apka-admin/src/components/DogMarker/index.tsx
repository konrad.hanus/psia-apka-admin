import {  Marker, Popup } from 'react-leaflet';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
// @ts-ignore
import toast from 'react-hot-toast';
import removeItem from './helpers/removeItem';
import { MarkerType } from '../Map/types';

interface DogMarkerInterface {
    loading: boolean;
    snapshots: any;
    icon: any;
    type: MarkerType;
}

const DogMarker = (props: DogMarkerInterface) =>
    !props.loading &&
    props.snapshots &&
    props.snapshots.map((v:any) => {

      const marker = v.val();

    const onClickRemove = () => {
      removeItem(props.type, marker.uuid); 
    }
      
      return (<Marker
                key={marker.location.timestamp}
                position={[marker.location.latitude, marker.location.longitude]}
                icon={props.icon}
                 >
                  <Popup className={'popup'}>
                  <Box
        sx={{
            display: 'flex',
            border: 0,
            borderColor: 'black',
            flexDirection: 'column',
            alignItems: 'center',
            '& > *': {
            m: 1,
            },
        }}
        ><div 
            aria-label="large button group"
            style={{
              display: "flex",
              flexDirection: "column",
            }}>
            
            <Button 
              onClick={onClickRemove}
              key="one" 
              variant="contained" 
              style={{ margin: 5}}
            >Usuń</Button>
        </div>
                  </Box>
                  </Popup>
                 </Marker>);
    })

  export default DogMarker;