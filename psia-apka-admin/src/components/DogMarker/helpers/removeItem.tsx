import { ref, getDatabase, remove } from 'firebase/database';
import { app as firebaseApp } from '../../../firebase';
// @ts-ignore
import toast from 'react-hot-toast';

const database = getDatabase(firebaseApp);
function removeItem(
    marker: string,
    uuidKey: string,
    ) {      
    remove(ref(database, 'public/' + marker + '/'+ uuidKey))
    .then(() => {
      toast.success(`Twój ${marker} został usunięty`)
    })
    .catch((error) => {
      toast.error('Nie usunięto')
    });
  }
  export default removeItem;